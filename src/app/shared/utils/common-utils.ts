export const clean = (number) => {
    return number
        .toString()
        .replace(/[^\d\^\+]/gm, '');
};