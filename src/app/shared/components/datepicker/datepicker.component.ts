import { Component, forwardRef, Input, Renderer2, ElementRef, OnChanges, SimpleChanges, ViewChild, Output, EventEmitter } from "@angular/core";
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from "@angular/forms";
import { DatePipe } from "@angular/common";
import { MatDatepickerInputEvent } from "@angular/material";

@Component({
    selector: 'app-datepicker',
    templateUrl: './datepicker.component.html',
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DatePickerComponent),
            multi: true
        }
    ]
})
export class DatePickerComponent implements OnChanges, ControlValueAccessor {
    private _DEFAULT_DATE_FORMAT: string = 'yyyy-MM-dd';
    private _VALUE: any;

    constructor(private renderer: Renderer2, private datePipe: DatePipe) { }

    @Input() dateFormat: string = this._DEFAULT_DATE_FORMAT;
    @Input() disable: boolean = false;
    @Output() changeEvent: EventEmitter<string> = new EventEmitter<string>();
    @ViewChild('input', {static: false}) inputEl: ElementRef;

    get dateValue() {
        return this.datePipe.transform(this._VALUE, this.dateFormat);
    }

    set dateValue(val) {
        this._VALUE = this.datePipe.transform(val, this.dateFormat);
        this.propagateChange(this._VALUE);
    }

    ngOnChanges(changes: SimpleChanges): void {
        if (changes.disable != null) {
            if (changes.disable.currentValue) {
                this.setDisabledState(true);
            }
        }
    }

    onInputEvent(event: MatDatepickerInputEvent<Date>) {
        this.dateValue = this.datePipe.transform(event.value, this.dateFormat);
    }

    onChangeEvent() {
        this.changeEvent.emit(this._VALUE);
    }

    writeValue(value: any) {
        if (value !== undefined) {
            this.dateValue = this.datePipe.transform(value, this.dateFormat);
        }
    }

    registerOnChange(fn) {
        this.propagateChange = fn;
        this.dateValue = this.datePipe.transform(this._VALUE, this.dateFormat);
    }

    registerOnTouched() { }

    setDisabledState(isDisabled: boolean): void {
        this.renderer.setProperty(this.inputEl.nativeElement, 'disabled', isDisabled);
    }

    propagateChange = (_: any) => { };
}