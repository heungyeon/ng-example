import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'ellipsis'
})
export class EllipsisPipe implements PipeTransform {
    suffix: string = '...';

    transform(value: string, limit: number): string {
        if (!value || (value && value.trim().length == 0)) {
            return '';
        }
        return value.length > limit ? value.slice(0, limit - 3).concat(this.suffix) : value;
    }
}
