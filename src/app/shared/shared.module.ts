import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { DatePickerComponent } from "./components/datepicker/datepicker.component";
import { MatDatepickerModule } from "@angular/material";

@NgModule({
  declarations: [DatePickerComponent],
  imports: [CommonModule, MatDatepickerModule],
})
export class SharedModule {}
